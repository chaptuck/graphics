
"use strict";

var canvas;
var gl;

var axis;
var floor;
var segment;
var sphere;
var texture;

var lineProgram;
var floorProgram;
var sphereProgram;

var mouseDown;

// A = angle
// X = x-axis
// Y = y-axis
// Z = z-axis
var view = "A";
var aspect = 1.0;
var eyeZ = 5;
var initialY;
var deltaZoom = 0.1;

var mvMatrix, pMatrix, nMatrix;

var p, q;

var pList = [];
var curList = [];
var rotationList = [];

function arcRotate(P, Q){
    var R = vec3(0.0,0.0,0.0);
    var P = vec3(P,0.0);
    var p_vec = subtract(P, R);
    
    var Q = vec3(Q, 0.0);
    var q_vec = subtract(Q, R);
    
    var rot_vec = cross(p_vec,q_vec);

    var rot_theta = dot(p_vec, q_vec);
    
    rot_theta = Math.acos(rot_theta);
    var rot_matrix = rotate(rad2deg(rot_theta), rot_vec);
    return rot_matrix;

}

// Mouse stuff
function onMouseDown(e){
    var pos = win2gl(event.clientX, event.clientY);
    p = pos;
    //console.log(p);
    mouseDown = true;
    e = e || window.event;
    switch (e.which){
    case 1:
	
	break;
    case 3:
	initialY = event.clientY;
	break;
    }
}

function onMouseMove(e){
    e = e || window.event;
    
    if(mouseDown){
	switch (e.which){
	case 1:
	    q = win2gl(event.clientX, event.clientY);
	    rotationList.pop();
	    rotationList.push(arcRotate(p,q));
	    render();
	    break;
	case 3:
	    if(event.clientY > initialY){
		if (eyeZ > 0){ eyeZ = eyeZ - deltaZoom; }
		render();
	    } else{
		eyeZ = eyeZ + deltaZoom;
		
		render();
	    }
	    initialY = event.clientY;
	    break;
	}
    }
}

function onMouseUp(e){
    e = e || window.event;
    
    if(mouseDown){
	switch (e.which){
	case 1:
	    q = win2gl(event.clientX, event.clientY);
	    rotationList.pop();
	    rotationList.push(arcRotate(p,q));
	    render();
	    
	    break;
	case 3:
	    initialY = null;
	    break;
	}
    }
    
    mouseDown = false;

}

function rad2deg(theta){
    return theta * 180 / Math.PI;
}

function win2gl(x, y){
    var canvas = document.getElementById("gl-canvas");
    var w = canvas.width;
    var h = canvas.height;
    var z;
    x = x - canvas.offsetLeft;
    x = 2.0*x/w-1.0;
    y = y - canvas.offsetTop;
    y = 2.0*(h-y-1)/h-1.0;
    z = (1 - (Math.abs(x) + Math.abs(y)));

    return vec3(x, y, z);
}


// Stack stuff
var matrixStack = new Array();
function pushMatrix() {
    matrixStack.push(mat4(mvMatrix));
}
function popMatrix() {
    mvMatrix = matrixStack.pop();
}

var LineProgram = function() {
    this.program = initShaders(gl, "line-vshader", "line-fshader");
    gl.useProgram(this.program);

    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");
    this.vTexCoord = gl.getAttribLocation(this.program, "vTexCoord");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
    this.pMatrixLoc = gl.getUniformLocation(this.program, "pMatrix");
    this.nMatrixLoc = gl.getUniformLocation(this.program, "nMatrix");
    
}

var FloorProgram = function() {
    this.program = initShaders(gl, "floor-vshader", "floor-fshader");
    gl.useProgram(this.program);

    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");
    this.textureLoc = gl.getAttribLocation(this.program, "vTexCoord");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
    this.pMatrixLoc = gl.getUniformLocation(this.program, "pMatrix");
    this.nMatrixLoc = gl.getUniformLocation(this.program, "nMatrix");
    
}

var SphereProgram = function() {
    this.program = initShaders(gl, "sphere-vshader", "sphere-fshader");
    gl.useProgram(this.program);

    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.normalLoc = gl.getAttribLocation(this.program, "vNormal");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
    this.pMatrixLoc = gl.getUniformLocation(this.program, "pMatrix");
    this.nMatrixLoc = gl.getUniformLocation(this.program, "nMatrix");
}


function renderAxis() {
    gl.useProgram(lineProgram.program);

    gl.enableVertexAttribArray(lineProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, axis.vertexBuffer);
    gl.vertexAttribPointer(lineProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(lineProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, axis.colorBuffer);
    gl.vertexAttribPointer(lineProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(lineProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(lineProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.drawArrays(gl.LINES, 0, axis.numPoints);
};

function renderFloor() {
    gl.useProgram(floorProgram.program);

    gl.enableVertexAttribArray(floorProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.vertexBuffer);
    gl.vertexAttribPointer(floorProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(floorProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.colorBuffer);
    gl.vertexAttribPointer(floorProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);
    
    gl.enableVertexAttribArray(floorProgram.textureLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.texBuffer);
    gl.vertexAttribPointer(floorProgram.textureLoc, 2, gl.FLOAT, false, 0, 0);
    
    
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.texBuffer);
    
    gl.uniformMatrix4fv(floorProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(floorProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, floor.texSize, floor.texSize, 0, gl.RGB, gl.UNSIGNED_BYTE, floor.normals);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                     gl.NEAREST_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
       
    gl.drawArrays(gl.TRIANGLE_FAN, 0, floor.numPoints);
};

function renderSegment() {
    gl.useProgram(lineProgram.program);

    gl.enableVertexAttribArray(lineProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, segment.vertexBuffer);
    gl.vertexAttribPointer(lineProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(lineProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, segment.colorBuffer);
    gl.vertexAttribPointer(lineProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(lineProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(lineProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.drawArrays(gl.LINES, 0, segment.numPoints);
};

function renderSphere() {
    gl.useProgram(sphereProgram.program);

    gl.enableVertexAttribArray(sphereProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.vertexBuffer);
    gl.vertexAttribPointer(sphereProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(sphereProgram.normalLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.normalBuffer);
    gl.vertexAttribPointer(sphereProgram.normalLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(sphereProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.colorBuffer);
    gl.vertexAttribPointer(sphereProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    nMatrix = normalMatrix(mvMatrix, false);

    gl.uniformMatrix4fv(sphereProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(sphereProgram.pMatrixLoc, false, flatten(pMatrix));
    gl.uniformMatrix4fv(sphereProgram.nMatrixLoc, false, flatten(nMatrix));

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphere.indexBuffer);
    gl.drawElements(gl.TRIANGLES, sphere.numIndices, gl.UNSIGNED_SHORT, 0);
};

function configureTexture(image) {
    texture = gl.createTexture();
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB,
                  gl.RGB, gl.UNSIGNED_BYTE, image);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER,
                     gl.NEAREST_MIPMAP_LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);

    gl.uniform1i(gl.getUniformLocation(floorProgram.program, "texture"), 0);
}

function tick() {
    //requestAnimFrame(tick);
    render();
    theta += 0.01;
}

var theta = radians(45);
function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    const fovy = 40.0;
    const near = 0.01;
    const far = 100;
    const radius = 5;
    const at = vec3(0.0, 0.0, 0.0);
    var up = vec3(0.0, 1.0, 0.0);
    var eye = vec3(0.0,1.0,eyeZ);
    //console.log(eyeZ);
    
    pMatrix = perspective(fovy, aspect, near, far);
    mvMatrix = lookAt(eye, at , up);
    pushMatrix();
    //renderFloor();

    //mvMatrix = mult(mvMatrix, scalem(0.5, 0.5, 0.5));
    for (var i = 0; i < rotationList.length; i++){
	mvMatrix = mult(mvMatrix, rotationList[i]);
	// console.log(i);
    }
    pushMatrix();
    mvMatrix = mult(mvMatrix, scalem(0.5,0.5,0.5));
    //renderSphere();
    popMatrix();

    pushMatrix();
    mvMatrix = mult(mvMatrix, translate(0.5,0.5,0.5,0));
    mvMatrix = mult(mvMatrix, scalem(0.1,0.1,0.1));
    renderSphere();
    popMatrix();
		    
    pushMatrix();
    mvMatrix = mult(mvMatrix, translate(-0.5,-0.5,-0.5,0));
    mvMatrix = mult(mvMatrix, scalem(0.1,0.1,0.1));
    renderSphere();
    popMatrix();

    pushMatrix();
    mvMatrix = mult(mvMatrix, translate(0.5,-0.5,0.5,0));
    mvMatrix = mult(mvMatrix, scalem(0.1,0.1,0.1));
    renderSphere();
    popMatrix();

    pushMatrix();
    mvMatrix = mult(mvMatrix, translate(-0.5,0.5,-0.5,0));
    mvMatrix = mult(mvMatrix, scalem(0.1,0.1,0.1));
    renderSphere();
    popMatrix();

    pushMatrix();
    mvMatrix = mult(mvMatrix, rotateX(180));
    mvMatrix = mult(mvMatrix, rotateY(180));
    renderAxis();
    popMatrix();
    
    renderAxis();
    renderFloor();
    //renderSphere();
    popMatrix();
    
}

function keyDown(e) {
    switch (e.keyCode) {
    case 37:
	// left arrow
	break;
    case 38:
	// up arrow
	break;
    case 39:
	// right arrow
	break;
    case 40:
	// down arrow
	break;
    case "X".charCodeAt(0):
	view = "X";
	break;
    case "Y".charCodeAt(0):
	view = "Y";
	break;
    case "Z".charCodeAt(0):
	view = "Z";
	break;
    case "A".charCodeAt(0):
	view = "A";
	break;
    default:
	//console.log("Unrecognized key press: " + e.keyCode);
	break;
    }

    requestAnimFrame(render);
}

window.onload = function init() {
    document.onkeydown = keyDown;
    
    canvas = document.getElementById("gl-canvas");
    canvas.addEventListener("mousedown", onMouseDown);
    canvas.addEventListener("mousemove", onMouseMove);
    canvas.addEventListener("mouseup", onMouseUp);
    canvas.addEventListener("mouseout", onMouseUp);

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) { alert("WebGL isn't available"); }

    gl.viewport(0, 0, canvas.width, canvas.height);

    aspect =  canvas.width/canvas.height;

    gl.clearColor(0.5, 0.5, 0.5, 1.0);

    gl.enable(gl.DEPTH_TEST);

    axis = new Axis();
    floor = new Floor();
    segment = new Segment();
    sphere = new Sphere(1, 200, 200);

    //  Load shaders and initialize attribute buffers
    lineProgram = new LineProgram();
    floorProgram = new FloorProgram();
    sphereProgram = new SphereProgram();

    // Initialize texture
    gl.useProgram(floorProgram.program);
    //var image = document.getElementById("texImage");
    
    var image = new Image();
    image.src = "tux_linux_inside_512.jpg"
    image.onload = function() {
	configureTexture(image);
    }

    configureTexture(image);
    
    tick();
}
