var Floor = function() {
    var pointsArray = [];
    var colorsArray = [];
    var y = 0.0;
    var lo = -1.0;
    var hi = 1.0;
    var inc = 0.1;
    for (var x = lo; x <= hi; x += inc) {
	pointsArray.push(vec4(x, y, hi));
	pointsArray.push(vec4(x, y, lo));
	colorsArray.push(vec4(0.0, 0.0, 0.0, 1.0));
	colorsArray.push(vec4(0.0, 0.0, 0.0, 1.0));
    }
    for (var z = lo; z <= hi; z += inc) {
	pointsArray.push(vec4(lo, y, z));
	pointsArray.push(vec4(hi, y, z));
	colorsArray.push(vec4(0.0, 0.0, 0.0, 1.0));
	colorsArray.push(vec4(0.0, 0.0, 0.0, 1.0));
    }
    pointsArray = [];
    colorsArray = [];
    var texCoordsArray = [];
    
    vertices = [
	vec4(-1.0, 0.0, -1.0, 1.0),
	vec4(1.0, 0.0, -1.0, 1.0),
	vec4(1.0, 0.0, 1.0, 1.0),
	vec4(-1.0, 0.0, 1.0, 1.0)
    ]
    colorsArray = [
	vec4(0.25, 0.25, 0.25,1.0),
	vec4(0.25, 0.25, 0.25,1.0),
	vec4(0.25, 0.25, 0.25,1.0),
	vec4(0.25, 0.25, 0.25,1.0),
	vec4(0.25, 0.25, 0.25,1.0),
	vec4(0.25, 0.25, 0.25,1.0),
    ]

    var texCoord = [
	vec2(0,0),
	vec2(1,0),
	vec2(1,1),
	vec2(0,1)
    ]

    pointsArray.push(vertices[0]); 
    texCoordsArray.push(texCoord[0]);

    pointsArray.push(vertices[1]); 
    texCoordsArray.push(texCoord[1]); 

    pointsArray.push(vertices[2]); 
    texCoordsArray.push(texCoord[2]); 
    
    pointsArray.push(vertices[2]); ;
    texCoordsArray.push(texCoord[2]); 

    pointsArray.push(vertices[3]); 
    texCoordsArray.push(texCoord[3]); 

    pointsArray.push(vertices[0]); 
    texCoordsArray.push(texCoord[0]);   
    

    
    var texSize = 256;
    // Bump Data
    var data = new Array()
    for (var i = 0; i<= texSize; i++)  data[i] = new Array();
    for (var i = 0; i<= texSize; i++) for (var j=0; j<=texSize; j++) 
        data[i][j] = 0.0;
    for (var i = texSize/4; i<3*texSize/4; i++) for (var j = texSize/4; j<3*texSize/4; j++)
        data[i][j] = 1.0;

    // Bump Map Normals
    var normalst = new Array()
    for (var i=0; i<texSize; i++)  normalst[i] = new Array();
    for (var i=0; i<texSize; i++) for ( var j = 0; j < texSize; j++) 
        normalst[i][j] = new Array();
    for (var i=0; i<texSize; i++) for ( var j = 0; j < texSize; j++) {
        normalst[i][j][0] = data[i][j]-data[i+1][j];
        normalst[i][j][1] = data[i][j]-data[i][j+1];
        normalst[i][j][2] = 1;
    }

    // Scale to Texture Coordinates
    for (var i=0; i<texSize; i++) for (var j=0; j<texSize; j++) {
	var d = 0;
	for(k=0;k<3;k++) d+=normalst[i][j][k]*normalst[i][j][k];
	d = Math.sqrt(d);
	for(k=0;k<3;k++) normalst[i][j][k]= 0.5*normalst[i][j][k]/d + 0.5;
    }

    // Normal Texture Array

    var normals = new Uint8Array(3*texSize*texSize);

    for ( var i = 0; i < texSize; i++ )
        for ( var j = 0; j < texSize; j++ ) 
            for(var k =0; k<3; k++) 
                normals[3*texSize*i+3*j+k] = 255*normalst[i][j][k];


    this.vertexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(pointsArray), gl.STATIC_DRAW);

    this.colorBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.colorBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(colorsArray), gl.STATIC_DRAW);


    this.texBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, this.texBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(texCoordsArray), gl.STATIC_DRAW);

    this.numPoints = pointsArray.length;
    
}

