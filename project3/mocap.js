"use strict";

var canvas;
var gl;

// Viewing
var aspect = 1.0;

// Data
var bvh;

// Programs
var lineProgram;
var sphereProgram;

// View
// A = angle
// X = x-axis
// Y = y-axis
// Z = z-axis
var view = "A";
var aspect = 1.0;

// Camera
var fovy = 10.0;
var near = 0.01;
var far = 100;
var radius = 5;
var theta = radians(45);
var at = vec3(0.0, 0.25, 0.0);
var delta_fovy = 1;
var delta_theta = 0.1;
var follow = false;

// Objects
var axis;
var floor;
var segment;
var sphere;

var mvMatrix, pMatrix, nMatrix;

var report = false;

var total_frame = 0;
var current_frame = 0;
var frame_delta = 0.0001

// Animation and time tracking
var animation = false;
var d = new Date();
var n = d.getTime();
var last_render = n;

// Stack stuff
var matrixStack = new Array();

function pushMatrix() {
    matrixStack.push(mat4(mvMatrix));
}

function popMatrix() {
    mvMatrix = matrixStack.pop();
}

function animate() {
    if (animation) {
        var newD = new Date();
        var newN = newD.getTime();
        var delta = newN - last_render;
        if (Math.floor(delta / 1000 / bvh.frameTime) >= 1) {
            current_frame = current_frame + Math.floor(delta / 1000 / bvh.frameTime);
            if (current_frame > total_frame - 1) {
                current_frame = 0;

            }
            last_render = newN;
        }
    }
}

function tick() {
    requestAnimFrame(tick);
    render();
    animate();
}

var LineProgram = function() {
    this.program = initShaders(gl, "line-vshader", "line-fshader");
    gl.useProgram(this.program);

    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
    this.pMatrixLoc = gl.getUniformLocation(this.program, "pMatrix");
    this.nMatrixLoc = gl.getUniformLocation(this.program, "nMatrix");
}

var SphereProgram = function() {
    this.program = initShaders(gl, "sphere-vshader", "sphere-fshader");
    gl.useProgram(this.program);

    this.vertexLoc = gl.getAttribLocation(this.program, "vPosition");
    this.normalLoc = gl.getAttribLocation(this.program, "vNormal");
    this.colorLoc = gl.getAttribLocation(this.program, "vColor");

    this.mvMatrixLoc = gl.getUniformLocation(this.program, "mvMatrix");
    this.pMatrixLoc = gl.getUniformLocation(this.program, "pMatrix");
    this.nMatrixLoc = gl.getUniformLocation(this.program, "nMatrix");
}

var reader = new FileReader();
reader.onload = function(e) {
    var text = reader.result;
}

function renderAxis() {
    gl.useProgram(lineProgram.program);

    gl.enableVertexAttribArray(lineProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, axis.vertexBuffer);
    gl.vertexAttribPointer(lineProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(lineProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, axis.colorBuffer);
    gl.vertexAttribPointer(lineProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(lineProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(lineProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.drawArrays(gl.LINES, 0, axis.numPoints);
};

function renderFloor() {
    gl.useProgram(lineProgram.program);

    gl.enableVertexAttribArray(lineProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.vertexBuffer);
    gl.vertexAttribPointer(lineProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(lineProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, floor.colorBuffer);
    gl.vertexAttribPointer(lineProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(lineProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(lineProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.drawArrays(gl.LINES, 0, floor.numPoints);
};

function renderSegment() {
    gl.useProgram(lineProgram.program);

    gl.enableVertexAttribArray(lineProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, segment.vertexBuffer);
    gl.vertexAttribPointer(lineProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(lineProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, segment.colorBuffer);
    gl.vertexAttribPointer(lineProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    gl.uniformMatrix4fv(lineProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(lineProgram.pMatrixLoc, false, flatten(pMatrix));

    gl.drawArrays(gl.LINES, 0, segment.numPoints);
};

function renderSphere() {
    gl.useProgram(sphereProgram.program);

    gl.enableVertexAttribArray(sphereProgram.vertexLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.vertexBuffer);
    gl.vertexAttribPointer(sphereProgram.vertexLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(sphereProgram.normalLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.normalBuffer);
    gl.vertexAttribPointer(sphereProgram.normalLoc, 4, gl.FLOAT, false, 0, 0);

    gl.enableVertexAttribArray(sphereProgram.colorLoc);
    gl.bindBuffer(gl.ARRAY_BUFFER, sphere.colorBuffer);
    gl.vertexAttribPointer(sphereProgram.colorLoc, 4, gl.FLOAT, false, 0, 0);

    nMatrix = normalMatrix(mvMatrix, false);

    gl.uniformMatrix4fv(sphereProgram.mvMatrixLoc, false, flatten(mvMatrix));
    gl.uniformMatrix4fv(sphereProgram.pMatrixLoc, false, flatten(pMatrix));
    gl.uniformMatrix4fv(sphereProgram.nMatrixLoc, false, flatten(nMatrix));

    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, sphere.indexBuffer);
    gl.drawElements(gl.TRIANGLES, sphere.numIndices, gl.UNSIGNED_SHORT, 0);
};

function traverse(id) {
    if (id == null) return;
    
    var x = id.offsets[0];
    var y = id.offsets[1];
    var z = id.offsets[2];

    var lenSeg = Math.sqrt(Math.pow(x,2) + Math.pow(y, 2) + Math.pow(z, 2));

    var rotTheta = 0;
    rotTheta = -rad2deg(Math.acos(dot(normalize(vec3(x, y, z)), vec3(1, 0, 0))));

    // Draw segment
    pushMatrix();
    if (x != 0 || y != 0 || z != 0) {
        var rotAxis = cross(normalize(vec3(x, y, z)), vec3(1, 0, 0));

        mvMatrix = mult(mvMatrix, rotate(rotTheta, rotAxis));
        mvMatrix = mult(mvMatrix, scalem(lenSeg, lenSeg, lenSeg));

        renderSegment();
    }

    popMatrix();

    // Translate 
    pushMatrix();
    mvMatrix = mult(mvMatrix, translate(x, y, z));

    if (id.name === "Hips") { // Position the model in the frame - The hip has the position values as well as rotation
        mvMatrix = mult(mvMatrix, translate(bvh.frames[current_frame][0], bvh.frames[current_frame][1], bvh.frames[current_frame][2]));
        if (follow){
            at = vec3(bvh.frames[current_frame][0]*.01, bvh.frames[current_frame][1]*.01, bvh.frames[current_frame][2]*.01);
        } else {
            at = vec3(0.0, 0.25, 0.0);
        }
        mvMatrix = mult(mvMatrix, rotateZ(bvh.frames[current_frame][3]));
        mvMatrix = mult(mvMatrix, rotateY(bvh.frames[current_frame][4]));
        mvMatrix = mult(mvMatrix, rotateX(bvh.frames[current_frame][5]));
    } else { // Rotation values can be in any order so check the order the apply rotation
        if (id.channels[0] === "Zrotation") {
            mvMatrix = mult(mvMatrix, rotateZ(bvh.frames[current_frame][id.channelOffset]));
        } else if (id.channels[0] === "Yrotation") {
            mvMatrix = mult(mvMatrix, rotateY(bvh.frames[current_frame][id.channelOffset]));
        } else {
            mvMatrix = mult(mvMatrix, rotateX(bvh.frames[current_frame][id.channelOffset]));
        }

        if (id.channels[1] === "Zrotation") {
            mvMatrix = mult(mvMatrix, rotateZ(bvh.frames[current_frame][id.channelOffset + 1]));
        } else if (id.channels[1] === "Yrotation") {
            mvMatrix = mult(mvMatrix, rotateY(bvh.frames[current_frame][id.channelOffset + 1]));
        } else {
            mvMatrix = mult(mvMatrix, rotateX(bvh.frames[current_frame][id.channelOffset + 1]));
        }

        if (id.channels[2] === "Zrotation") {
            mvMatrix = mult(mvMatrix, rotateZ(bvh.frames[current_frame][id.channelOffset + 2]));
        } else if (id.channels[2] === "Yrotation") {
            mvMatrix = mult(mvMatrix, rotateY(bvh.frames[current_frame][id.channelOffset + 2]));
        } else {
            mvMatrix = mult(mvMatrix, rotateX(bvh.frames[current_frame][id.channelOffset + 2]));
        }

    }

    // Scale and draw the sphere
    pushMatrix();
    mvMatrix = mult(mvMatrix, scalem(0.85, 0.85, 0.85));
    renderSphere();
    popMatrix();

    // Recursivly draw all joints and segments
    if (id.children.length > 0) {
        var len = id.children.length;
        for (var i = 0; i < len; i++) {
            pushMatrix();
            traverse(id.children[i]);
            popMatrix();
        }
    }
    popMatrix();

}

function render() {
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    var up = vec3(0.0, 1.0, 0.0);
    var eye = vec3(radius * Math.sin(theta),
        radius * Math.sin(radians(5.0)),
        radius * Math.cos(theta));
    if (view == "X") {
        eye = vec3(5, 0, 0);
    } else if (view == "Y") {
        eye = vec3(0, 5, 0);
        up = vec3(0.0, 0.0, -1.0);
    } else if (view == "Z") {
        eye = vec3(0, 0, 5);
    }

    pMatrix = perspective(fovy, aspect, near, far);
    mvMatrix = lookAt(eye, at, up);

    renderFloor();

    mvMatrix = mult(mvMatrix, scalem(.01, .01, .01));
    traverse(bvh.roots[0]);

}

/**
 * Parses a BVH file and places the result in the bvh variable.
 */
function parse(input) {
    var antlr4 = require('antlr4/index');
    var BVHLexer = require('parser/BVHLexer');
    var BVHListener = require('parser/BVHListener');
    var BVHParser = require('parser/BVHParser');
    require('./BVH');

    var chars = new antlr4.InputStream(input);
    var lexer = new BVHLexer.BVHLexer(chars);
    var tokens = new antlr4.CommonTokenStream(lexer);
    var parser = new BVHParser.BVHParser(tokens);
    parser.buildParseTrees = true;
    var tree = parser.mocap();

    bvh = new BVH();
    antlr4.tree.ParseTreeWalker.DEFAULT.walk(bvh, tree);

    // TODO: add any initialization code upon loading a BVH file
    current_frame = 0;
    total_frame = bvh.numFrames;

    console.log(bvh);
}

function keyDown(e) {
    switch (e.keyCode) {
        case 37:
            // left
            theta = theta - delta_theta;
            break;
        case 38:
            // up
            if(fovy > 1){
                fovy = fovy - delta_fovy;
            }
            
            break;
        case 39:
            // right
            theta = theta + delta_theta;
            break;
        case 40:
            // down
            if(fovy < 150){
                fovy = fovy + delta_fovy;
            }
            break;
        case 65: 
            // A 
            bvh.frameTime = bvh.frameTime + frame_delta;
            document.getElementById("fps").innerHTML = Math.floor(1 / bvh.frameTime);
            break;
        case 68: 
            // D
            bvh.frameTime = bvh.frameTime - frame_delta;
            document.getElementById("fps").innerHTML = Math.floor(1 / bvh.frameTime);

            break;
        case 32:
            // spacebar
            animation = !animation;
            d = new Date();
            n = d.getTime();
            last_render = n;
            break;
        case "F".charCodeAt(0):
            // F or f
            break;
        case 85:
            if (current_frame < total_frame - 1) {
                current_frame = current_frame + 1
            } else {
                current_frame = 0;
            }
            break;
        case 88:
            if (current_frame > 0) {
                current_frame = current_frame - 1
            } else {
                current_frame = total_frame - 1;
            }
            break;
        default:
            // To see what the code for a certain key is, uncomment this line,
            // reload the page in the browser and press the key.
            console.log("Unrecognized key press: " + e.keyCode);
            break;
    }
}

window.onload = function init() {
    document.onkeydown = keyDown;

    canvas = document.getElementById("gl-canvas");
    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    gl.viewport(0, 0, canvas.width, canvas.height);
    aspect = canvas.width / canvas.height;

    // Awesome green background.
    gl.clearColor(0.9, 0.9, 0.9, 1.0);
    gl.enable(gl.DEPTH_TEST);

    //  Load shaders and initialize attribute buffers
    axis = new Axis();
    floor = new Floor();
    segment = new Segment();
    sphere = new Sphere(1, 20, 20);

    //  Load shaders and initialize attribute buffers
    lineProgram = new LineProgram();
    sphereProgram = new SphereProgram();

    // Listen for a file to be loaded and parse
    var fileInput = document.getElementById('fileInput');
    fileInput.addEventListener('change', function(e) {
        var file = fileInput.files[0];
        if (file && file.name) {
            if (file.name.match(/.*\.bvh/)) {
                var reader = new FileReader();
                reader.onload = function(e) {
                    parse(reader.result);
                }
                reader.readAsText(file);
            } else {
                console.log("File not supported! " + file.type);
            }
        }
    });

    // Parse a default file.
    // TODO: change this to testData1 when you're ready to start rendering
    // animation. These strings are defined in test1.bvh.js and test2.bvh.js.
    parse(testData1);

    tick();
}


function rad2deg(rad) {
    var degree = rad * (180 / Math.PI);
    return degree;
}
