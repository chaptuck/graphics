/* 

Tucker Chapman
Advanced Computer Graphics
Project 1 - Visual Multiplication


*/

"use strict";

var gl;
var canvasWidth, canvasHeight;
var circleBufferId;
var colorBufferId;
var thetaId;
var modulusId;
var multiplyId;
var redId;
var greenId;
var blueId;

var red = 0;
var blue = 0;
var green = 0;

var theta = 0;
var modulus = 10;
var multiply = 2;
var radius = 90;
var circle_breaks = 360;

/* Here be dragons */

function scaleVertices(s, vertices) {
    for (var i = 0; i < vertices.length; ++i) {
        vertices[i] = scale(s, vertices[i]);
    }
}


window.onload = function init() {
    var canvas = document.getElementById("gl-canvas");
    canvasWidth = canvas.width;
    canvasHeight = canvas.height;

    gl = WebGLUtils.setupWebGL(canvas);
    if (!gl) {
        alert("WebGL isn't available");
    }

    window.onkeydown = function(event) {
        var key = String.fromCharCode(event.keyCode);
        switch (key) {
            case '&': // Up
                modulus += 1;
                document.getElementById("modulus").value= modulus;
                break;
            case '(': // Down
                if (modulus > 0) {
                    modulus -= 1;
                }
                document.getElementById("modulus").value = modulus;
                break;
            case '%': // Left
                if (multiply > 0) {
                    multiply -= 1;
                }
                document.getElementById("multiply").value = multiply;
                break;
            case '\'': //Right
                multiply += 1;
                document.getElementById("multiply").value = multiply;
                break;

        }
    };

    //
    //  Configure WebGL
    //
    gl.viewport(0, 0, canvasWidth, canvasHeight);
    gl.clearColor(0.9, 0.9, 0.9, 1.0);

    //  Load shaders and initialize attribute buffers

    var program = initShaders(gl, "vertex-shader", "fragment-shader");
    gl.useProgram(program);

    // Create circle buffer
    circleBufferId = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, circleBufferId);
    var vPosition = gl.getAttribLocation(program, "vPosition");
    gl.vertexAttribPointer(vPosition, 2, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    // Gather Uniform variables
    thetaId = gl.getUniformLocation(program, "theta");
    modulusId = gl.getUniformLocation(program, "modulus");
    multiplyId = gl.getUniformLocation(program, "multiply");
    redId = gl.getUniformLocation(program, "red");
    blueId = gl.getUniformLocation(program, "blue");
    greenId = gl.getUniformLocation(program, "green");

    render();
};

function render() {

    setTimeout(function() {
	// Connect uniform Ids to the local variables
        gl.uniform1f(modulusId, modulus);
        gl.uniform1f(multiplyId, multiply);
        gl.uniform1f(thetaId, theta);
        gl.uniform1f(redId, red);
        gl.uniform1f(blueId, blue);
        gl.uniform1f(greenId, green);

        gl.clear(gl.COLOR_BUFFER_BIT);

        // Load the vertices once
        var circle = createCircleTrig(radius, circle_breaks);

        var modulusPoints = createCircleTrig(radius, modulus);
        var lines = timesTables(modulus, multiply, modulusPoints);

        // Add Modules points to the circle array
        for (var i = 0; i < modulusPoints.length; i++) {
            circle.push(modulusPoints[i]);
        }

        // Add lines points to the circle array
        for (var i = 0; i < lines.length; i++) {
            circle.push(lines[i]);
        }

        scaleVertices(0.01, circle);
        gl.bufferData(gl.ARRAY_BUFFER, flatten(circle), gl.STATIC_DRAW);

        // Draw circle
        gl.drawArrays(gl.LINE_LOOP, 0, circle_breaks);

        // Draw Modules Points
        var points_start = circle_breaks + 1;
        gl.drawArrays(gl.POINTS, points_start, modulusPoints.length);

        // Draw Lines
        var line_start = circle_breaks + modulusPoints.length + 1;
        gl.drawArrays(gl.LINES, line_start, lines.length);

        requestAnimFrame(render);
    }, 50);
}


function createCircleTrig(radius = 1, num_breaks = 360, center_x = 0, center_y = 0) {
    // Returns an array with vertices to create a circle
    // @Params:
    // radius - enter the radius of the circle
    // breaks - number of vertices to create in the circle
    // center_x - the x center of the circle - default=0
    // center_y - the y center of the circle - default=0

    var step = (360 / num_breaks) * (Math.PI / 180);
    var vertices = []
    for (var i = 0; i < (2 * Math.PI); i = i + step) {
        var x = (Math.cos(i) * radius) + center_x;
        var y = (Math.sin(i) * radius) + center_y;
        vertices.push(vec2(x, y));
    }
    return vertices;
}


function timesTables(mo = 10, mu = 2, vertices) {
    // mo - the modules value
    // mu - the multiply value
    // vertices - an array of modules points used to create points for the lines
    // ### Consider putting a call to the createCircleTrig in this function instead of passing in as parameter ###
    var lines = []
    for (var i = 0; i < mo; i++) {
        var g = (mu * i) % mo;
        lines.push(vertices[i]);
        lines.push(vertices[g]);
    }

    return lines;
}

function updateRotation() {
    // Called to update the theta rotation value
    theta = document.getElementById("rotation").value;    
}

function updateColor() {
    // this function is called to update the RGB values
    red = document.getElementById("red_value").value;
    blue = document.getElementById("blue_value").value;
    green = document.getElementById("green_value").value;
}

function randomColor(){
    // Set the color to random values
    red = Math.random();
    document.getElementById("red_value").value = red;
    green = Math.random();
    document.getElementById("green_value").value = green;
    blue = Math.random();
    document.getElementById("blue_value").value = blue;
}

function updateModulus(){
    // Updates the modulus value to the entered value in the modulus field
    var newMod = parseFloat(document.getElementById("modulus").value);
    if(newMod < 0){
        newMod = Math.abs(newMod);
        
    } else if (isNaN(newMod)) {
        newMod = 10;
    }
    document.getElementById("modulus").value = newMod;
    modulus = newMod;
    
}
    
function updateMultiply(){
    // Updates the multiply value to the entered value in the multiply field
    var newMult = parseFloat(document.getElementById("multiply").value);
    if(newMult < 0){
        newMult = Math.abs(newMult);
        
    } else if (isNaN(newMult)) {
        newMult = 2;
    }
    
    document.getElementById("multiply").value = newMult;
    multiply = newMult;
}
